var networkInformation = function() {
    var networkInfo = {
        checkStatus    : 0,
        checkConnection: function() {
            //checking network connection\
            var networkState = navigator.connection.type;
            if (networkState === Connection.NONE) {
                if(networkInfo.checkStatus === 0) {
                    networkInfo.checkStatus = 1;
                    navigator.notification.alert(
                    'Unable to Connect with the Server.' +
                    ' Check out internet connection and try again',
                    networkInfo.manager,
                    'Connection error',
                    'Try again'
                );
            }
            } else {
                document.addEventListener("offline", networkInfo.checkConnection, false);
            }
        },
        manager: function() {
            networkInfo.checkStatus = 0;
            networkInfo.checkConnection();
        },

        onOnline: function() {
            // Handle the online event
            var networkState = navigator.connection.type;

            if (networkState !== Connection.NONE) {
                var states = {};
                states[Connection.UNKNOWN] = 'Unknown connection';
                states[Connection.ETHERNET] = 'Ethernet connection';
                states[Connection.WIFI] = 'WiFi connection';
                states[Connection.CELL_2G] = 'Cell 2G connection';
                states[Connection.CELL_3G] = 'Cell 3G connection';
                states[Connection.CELL_4G] = 'Cell 4G connection';
                states[Connection.CELL] = 'Cell generic connection';
                states[Connection.NONE] = 'No network connection';
            }
        }
    }

    return networkInfo;
}
