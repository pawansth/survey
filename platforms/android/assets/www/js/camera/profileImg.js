var profileImg = function() {
    var prof = {
        openDefaultCamera: function(selection) {
            var srcType  = Camera.PictureSourceType.CAMERA;
            var destType = Camera.DestinationType.FILE_URI;
            var options  = prof.setOptions(srcType, destType);
            // var func = createNewFileEntry;

            if (selection == "picker-thmb") {
                // To downscale a selected image,
                // Camera.EncodingType (e.g., JPEG) must match the selected image type.
                options.targetHeight = 100;
                options.targetWidth = 100;
            }
// imageUri
            navigator.camera.getPicture(function cameraSuccess(imageData) {
                prof.toDataURL(imageData);
            }, function cameraError(error) {
                console.debug("Unable to obtain picture: " + error, "app");
                $('.div-prev-img').hide();
            }, options);
        },
        openFilePicker: function(selection) {

            var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
            var destType = Camera.DestinationType.FILE_URI;
            var options = prof.setOptions(srcType, destType);

            if (selection == "picker-thmb") {
                // To downscale a selected image,
                // Camera.EncodingType (e.g., JPEG) must match the selected image type.
                options.targetHeight = 100;
                options.targetWidth = 100;
            }
            navigator.camera.getPicture(function cameraSuccess(imageData) {
                // Do something with image
                prof.toDataURL(imageData);
            }, function cameraError(error) {
                console.debug("Unable to obtain picture: " + error, "app");
                $('.div-prev-img').hide();
            }, options);
        },
        createNewFileEntry: function(imgUri) {
            window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {

                // JPEG file
                dirEntry.getFile("tempFile.jpeg", { create: true, exclusive: false }, function (fileEntry) {

                    // Do something with it, like write to it, upload it, etc.
                    // writeFile(fileEntry, imgUri);
                    console.log("got file: " + fileEntry.fullPath);
                    // displayFileData(fileEntry.fullPath, "File copied to");

                }, onErrorCreateFile);

            }, onErrorResolveUrl);
        },
        tempClean: function() {
            navigator.camera.cleanup(onSuccess, onFail);

            function onSuccess() {
                console.log("Camera cleanup success.")
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }
        },
        setOptions: function(srcType, destType) {
            var options = {
                // Some common settings are 20, 50, and 100
                quality: 50,
                destinationType: destType,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: srcType,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true,  //Corrects Android orientation quirks
            }
            return options;
        },
        toDataURL:function(src, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function() {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = '90';
                canvas.width = '90';
                var context = canvas.getContext("2d");
                context.scale(90/this.width,  90/this.height);
                context.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL('image/jpeg');
                $('.img-prev').attr('src','');
                $('.img-prev').attr('data','');
                $('.img-prev').attr('src',src);
                $('.img-prev').attr('data',dataURL);
                // $('.container-padding').hide();
                    $('.div-prev-img').show();
            };
            img.src = src;
            console.log(img);
        }
    }
    return prof;
}